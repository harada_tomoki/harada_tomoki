package harada_tomoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import harada_tomoki.beans.Branch;
import harada_tomoki.beans.Position;
import harada_tomoki.beans.User;
import harada_tomoki.service.BranchService;
import harada_tomoki.service.PositionService;
import harada_tomoki.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

		// 支店・部署・役職表示用
		List<Branch> branches  = new BranchService().getBranches();
		List<Position> positions = new PositionService().getPositions();

		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

//		// エラーメッセージ
		List<String> messages = new ArrayList<String>();

		// パスワード(確認用)の取得
		String validPassword = request.getParameter("validPassword");

		// ユーザー情報の設定
		User user = new User();

		user.setLoginName(request.getParameter("loginName"));
		user.setName(request.getParameter("name"));
		user.setPassword(request.getParameter("password"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setPositionId(Integer.parseInt(request.getParameter("positionId")));

		// このあたりにバリデーション
		if (isValid(messages, user, validPassword)){
			new UserService().register(user);
			response.sendRedirect("maintenance");
		}else{
			// 支店・部署・役職表示用
			List<Branch> branches  = new BranchService().getBranches();
			List<Position> positions = new PositionService().getPositions();

			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);
			request.setAttribute("errorMessages", messages);
			request.setAttribute("user", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}
	}

	private boolean isValid(List<String> messages,User user,String validPassword)
	{
		// パターン
		String loginNamePattern = "^[a-zA-Z0-9_\\-.]{6,20}$";
		String passwordPattern = "^(?=.[-_@+*;:#$%&\\w]).{6,20}$";

		// 検証対象
		String loginName = user.getLoginName();
		String password = user.getPassword();
		String name = user.getName();

		// 検証用変数
		Pattern pattern;
		Matcher matcher;

		// ログインIDの検証
		pattern = Pattern.compile(loginNamePattern);
		matcher = pattern.matcher(loginName);
		if(!matcher.matches()){
			messages.add("ログインIDは半角文字で6文字以上20文字以下で入力してください");
		}

		// SQL条件用のユーザーを生成
		User whereUser = new User();
		whereUser.setLoginName(loginName);
		if ((loginName.length() > 0) && (new UserService().getUsers(whereUser).size() > 0)){
			messages.add("入力されたログインIDは使用できません");
		}

		// パスワードの検証
		pattern = Pattern.compile(passwordPattern);
		matcher = pattern.matcher(password);
		if(!matcher.matches()){
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
		}

		if(!password.equals(validPassword)){
			messages.add("パスワードとパスワード(確認用)が一致しません");
		}

		// 名前の検証
		if (name.length() > 10){
			messages.add("名称は10文字以下で入力してください");
		}

		// エラーメッセージが0件より多いならエラー
		if (messages.size() > 0){
			return false;
		}

		return true;
	}
}
