<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="./semantic/semantic.css">
<link rel="stylesheet" type="text/css" href="./semantic/semantic.min.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/menu.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
<div class="ui top fixed menu">
	<a href="./" class="item"><i class="home icon"></i>ホーム</a>
	<div class="right menu">
		<a href="logout" class="item"><i class="sign out alternate icon"></i>ログアウト</a>
	</div>
</div>
<div class="main-contents">
<div class="error-contents">
<c:if test="${not empty errorMessages}">
	<div class="ui error message">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${ message }" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session" />
</c:if>
</div>
<div class="ui segment">
  <h2 class="ui left floated header">新規投稿</h2>
  <div class="ui clearing divider"></div>
<div class="contents-body">
	<form action="post" method="post" class="ui form">
	<div class="field">
	<label for="body">件名(30文字まで)</label>
	<input name="subject" id="subject" value="${ subject }">
	</div>
	<br />
	<div class="field">
	<label for="body">本文(1000文字まで)</label>
	<textarea name="body" cols="100" rows="10" class="tweet-box" ><c:out value="${ body }" /></textarea>
	</div>
	<div class="field">
	<label for="body">カテゴリ(10文字まで)</label>
	<input name="category" id="category" value="${ category }">
	</div>
	<div class="field">
	<div class="right">
		<button type="submit" class="ui teal button"><i class="pencil alternate icon"></i>投稿</button>
		<!-- <input type="submit" value="投稿" class="ui teal button"> -->
	</div>
	</div>
	</form>
<br />
</div>
</div>
<div class="copyright">Copyright(c)Tomoki Harada</div>
</div>
</body>
</html>