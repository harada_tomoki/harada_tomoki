package utils;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class CloseableUtil {
	public static void close(PreparedStatement ps){
		try{
			ps.close();
		}catch(Exception e){

		}
	}

	public static void close(InputStream is){
		try{
			is.close();
		}catch(Exception e){

		}
	}

	public static void close(Connection connection){
		try{
			connection.close();
		}catch(Exception e){

		}
	}

	public static void close(ResultSet rs){
		try{
			rs.close();
		}catch(Exception e){

		}
	}
}
