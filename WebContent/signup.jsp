<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="./semantic/semantic.min.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/menu.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
<div class="ui top fixed menu">
	<a href="./" class="item"><i class="home icon"></i>ホーム</a>
	<a href="maintenance" class="item"><i class="user icon"></i>ユーザー管理</a>
	<div class="right menu">
		<a href="logout" class="item"><i class="sign out alternate icon"></i>ログアウト</a>
	</div>
</div>
<div class="main-contents">
<div class="error-contents">
<c:if test="${ not empty errorMessages }">
	<div class="ui error message">
		<ul>
			<c:forEach items="${errorMessages}" var="message">
				<li><c:out value="${ message }" />
			</c:forEach>
		</ul>
	</div>
	<c:remove var="errorMessages" scope="session" />
</c:if>
</div>
<div class="ui segment">
  <h2 class="ui left floated header">ユーザー登録</h2>
  <div class="ui clearing divider"></div>
  <div class="contents-body">
<form action="signup" method="post" class="ui form">
	<div class="field">
	<label for="loginName">ログインID(半角英数6～20文字)</label>
	<input name="loginName" value="${ user.loginName }" id="loginName">
	</div>

	<div class="field">
	<label for="name">名称(10文字まで)</label>
	<input name="name" value="${ user.name }" id="name"/>
	</div>

	<div class="field">
	<label for="password">パスワード(記号を含む半角英数6～20文字)</label><br />
	<input name="password" type="password" id="password"><br />
	</div>

	<div class="field">
	<label for="validPassword">パスワード(確認用)</label>
	<input name="validPassword" type="password" id="validPassword">
	</div>

	<div class="field">
	<label for="branchId">支店</label>
	<select name="branchId" size="1">
		<c:forEach items="${ branches }" var="branch">
			<c:if test="${ branch.id == user.branchId }">
				<option value="${ branch.id }" selected><c:out value="${ branch.name }" /></option>
        	</c:if>
        	<c:if test="${ branch.id != user.branchId }">
				<option value="${ branch.id }"><c:out value="${ branch.name }" /></option>
			</c:if>
		</c:forEach>
	</select>
	</div>

	<div class="field">
	<label for="positionId">部署・役職</label>
	<select name="positionId" size="1">
		<c:forEach items="${ positions }" var="position">
			<c:if test="${ position.id == user.positionId }">
				<option value="${ position.id }" selected><c:out value="${ position.name }" /></option>
			</c:if>
			<c:if test="${ position.id != user.positionId }">
				<option value="${ position.id }"><c:out value="${ position.name }" /></option>
			</c:if>
		</c:forEach>
	</select>
	</div>
	<div class="field">
		<div class="right">
			<button type="submit" class="ui teal button"><i class="user circle icon"></i>登録</button>
			<!-- <div class="right"><input type="submit" value="登録"  class="ui teal button"/></div> -->
		</div>
	</div>
</form>
</div>
</div>
<div class="copyright">Copyright(c)Tomoki Harada</div>
</div>
</body>
</html>