package harada_tomoki.service;

import java.sql.Connection;
import java.util.List;

import harada_tomoki.beans.Comment;
import harada_tomoki.dao.CommentDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class CommentService {
	public boolean regist(Comment comment) {
		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			// DBへインサート
			CommentDao commentDao = new CommentDao();
			commentDao.insert(connection, comment);

			utils.DBUtil.commit(connection);

			return true;
		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			utils.DBUtil.rollback(connection);
			throw e;
		}finally{
			utils.CloseableUtil.close(connection);
		}
	}

	public boolean delete(int id){
		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			// DBへインサート
			CommentDao commentDao = new CommentDao();
			commentDao.delete(connection, id);

			utils.DBUtil.commit(connection);

			return true;
		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			utils.DBUtil.rollback(connection);
			throw e;
		}finally{
			utils.CloseableUtil.close(connection);
		}
	}

	public List<Comment> getComments() {
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();

			CommentDao commentDao = new CommentDao();
			List<Comment> comments = commentDao.getComments(connection);

			DBUtil.commit(connection);

			return comments;

		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}

	}
}
