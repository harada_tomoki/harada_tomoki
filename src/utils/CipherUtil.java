package utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.commons.codec.binary.Base64;

public class CipherUtil {
	public static String encrypt(String target){
		try{
			MessageDigest mg = MessageDigest.getInstance("SHA-256");
			mg.update(target.getBytes());

			return Base64.encodeBase64URLSafeString(mg.digest());

		}catch(NoSuchAlgorithmException e){
			throw new RuntimeException(e);
		}
	}

	public static String decode(String encoded) throws UnsupportedEncodingException {
		  byte[] buff = (new Base64()).decode(encoded);
		  return new String(buff, "utf-8");
		}
}
