package harada_tomoki.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import harada_tomoki.beans.User;
import harada_tomoki.exception.NoRowsUpdatedRuntimeException;
import harada_tomoki.exception.SQLRuntimeException;
import utils.CloseableUtil;

public class UserDao {
	public void insert(Connection connection,User user){

//		System.out.println("-- InsertStart --");
		PreparedStatement ps = null;
		try{

			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users (");
			sql.append("  login_name");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch_id");
			sql.append(", position_id");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(", is_deleted");
			sql.append(")VALUES(");
			sql.append("?");	// login_name
			sql.append(", ?");	// user_name
			sql.append(", ?");	// password
			sql.append(", ?");	// branch_id
			sql.append(", ?");	// position_id
			sql.append(", CURRENT_TIMESTAMP()");
			sql.append(", CURRENT_TIMESTAMP()");
			sql.append(",0)");

			//確認用
//			System.out.println(user.getLoginName());
//			System.out.println(user.getName());
//			System.out.println(user.getPassword());
//			System.out.println(user.getBranchId());
//			System.out.println(user.getPositionId());

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, user.getLoginName());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getPositionId());

//			System.out.println(sql.toString());

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
			utils.CloseableUtil.close(ps);
		}
	}

	public void update(Connection connection,User user){
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET ");
			sql.append("  name  = ? ");
			sql.append(", password = ? ");
			sql.append(", branch_id = ? ");
			sql.append(", position_id = ? ");
			sql.append(", update_date = CURRENT_TIMESTAMP() ");
			sql.append(", is_deleted = ? ");
			sql.append("WHERE login_name = ?");
			sql.append("  AND update_date = ?");	// account

			ps = connection.prepareStatement(sql.toString());
//			System.out.println(sql.toString());	//確認用

			ps.setString(1, user.getName());
			ps.setString(2, user.getPassword());
			ps.setInt(3, user.getBranchId());
			ps.setInt(4, user.getPositionId());
			ps.setInt(5, user.getIsDeleted());
			ps.setString(6, user.getLoginName());
			ps.setTimestamp(7, new Timestamp(user.getUpdateDate().getTime()));

			int count = ps.executeUpdate();
//			System.out.println("count:" + count);	//確認用

			if(count == 0){
				throw new NoRowsUpdatedRuntimeException();
			}
		}catch(SQLException e){
//			System.out.println(e.getMessage());	//確認用
			throw new SQLRuntimeException(e);
		}finally {
			CloseableUtil.close(ps);
		}
	}

	public List<User> getUsers(Connection connection,User user) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * FROM users ");

			// 渡されたユーザーを元に条件追加
			List<String> whereValueList = new ArrayList<String>();

			// id
			if (user.getId() != 0){
				sql.append("  AND id = ?");
				whereValueList.add(String.valueOf(user.getId()));
			}

			// ログインID
			if (user.getLoginName().length() != 0){
				sql.append("  AND login_name = ?");
				whereValueList.add(user.getLoginName());
			}

			// 名前
			if (user.getLoginName().length() != 0){
				sql.append("  AND name LIKE ?");
				whereValueList.add("%" + user.getName() + "%");
			}

			// 支店ID
			if (user.getBranchId() != 0){
				sql.append("  AND branch_id = ?");
				whereValueList.add(String.valueOf(user.getBranchId()));
			}

			// 部署・役職
			if (user.getPositionId() != 0){
				sql.append("  AND position_id = ?");
				whereValueList.add(String.valueOf(user.getPositionId()));
			}

			ps = connection.prepareStatement(sql.toString().replaceFirst("AND", "WHERE"));

//			System.out.println("whereValueList.size = " + whereValueList.size());//確認用

			for (int index = 1;index <= whereValueList.size();index++){
//				System.out.println("index:" + (index -1));
//				System.out.println("value:" + whereValueList.get(index -1));
				ps.setString(index, whereValueList.get(index -1));

			}

			ResultSet rs =ps.executeQuery();

			return toUserList(rs);

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			utils.CloseableUtil.close(ps);
		}
	}

	public List<User> getSortedUsers(Connection connection,List<String> sortKeys) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * FROM users ");
			sql.append("order by ");
			for (int i = 0;i < sortKeys.size();i++){
				sql.append(" ," + sortKeys.get(i));
			}

			ps = connection.prepareStatement(sql.toString().replaceFirst(",", ""));

			ResultSet rs =ps.executeQuery();

			return toUserList(rs);

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			utils.CloseableUtil.close(ps);
		}
	}

	private List<User> toUserList(ResultSet rs) throws SQLException{
		List<User> ret = new ArrayList<User>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String loginName = rs.getString("login_name");
				String name = rs.getString("name");
				String password = rs.getString("password");
				int branchId = rs.getInt("branch_id");
				int positionId = rs.getInt("position_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				Timestamp updateDate = rs.getTimestamp("update_date");
				int is_deleted = rs.getInt("is_deleted");

				User user = new User();
				user.setId(id);
				user.setLoginName(loginName);
				user.setName(name);
				user.setPassword(password);
				user.setBranchId(branchId);
				user.setPositionId(positionId);
				user.setInsertDate(insertDate);
				user.setUpdateDate(updateDate);
				user.setIsDeleted(is_deleted);

				ret.add(user);
			}
			return ret;
		}finally{
			utils.CloseableUtil.close(rs);
		}
	}

}
