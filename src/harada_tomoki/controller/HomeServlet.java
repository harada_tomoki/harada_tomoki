package harada_tomoki.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import harada_tomoki.beans.Comment;
import harada_tomoki.beans.Post;
import harada_tomoki.beans.User;
import harada_tomoki.service.CommentService;
import harada_tomoki.service.PostService;
import harada_tomoki.service.UserService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class HomeServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");

		String category = request.getParameter("category");
		String fromDate = request.getParameter("fromDate");;
		String toDate = request.getParameter("toDate");
//		String toDate = simpleDateFormat.format(new Date());

//		System.out.println("HomeServlet:doGet:category:" + category);
//		System.out.println("HomeServlet:doGet:fromDate:" + fromDate);
//		System.out.println("HomeServlet:doGet:toDate:" + toDate);

		// 表示用のデータを取得
		List<User> users = new UserService().getUsers(new User());
		List<Post> posts = new PostService().getPosts(category,fromDate,toDate);
		List<Comment> comments = new CommentService().getComments();
//		User loginUser = (User)session.getAttribute("loginUser");

		// 画面に渡す
		request.setAttribute("users", users);
		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("category", category);
		request.setAttribute("fromDate", fromDate);
		request.setAttribute("toDate", toDate);
//		request.setAttribute("loginUser", loginUser);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

		String category = request.getParameter("category");
		String fromDate = request.getParameter("fromDate");;
		String toDate = request.getParameter("toDate");

		if (fromDate.isEmpty()){
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
			fromDate = "2018-01-01";
			toDate = simpleDateFormat.format(new Date());
		}

		// 表示用のデータを取得
		List<User> users = new UserService().getUsers(new User());
		List<Post> posts = new PostService().getPosts(category,fromDate,toDate);
		List<Comment> comments = new CommentService().getComments();

		// 画面に渡す
		request.setAttribute("users", users);
		request.setAttribute("posts", posts);
		request.setAttribute("comments", comments);
		request.setAttribute("category", category);
		request.setAttribute("fromDate", fromDate);
		request.setAttribute("toDate", toDate);

		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}
}
