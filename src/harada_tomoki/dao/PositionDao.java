package harada_tomoki.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import harada_tomoki.beans.Position;
import harada_tomoki.exception.SQLRuntimeException;

public class PositionDao {

	public Position getPosition(Connection connection,String name){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM positions";
			ps = connection.prepareStatement(sql);

			ResultSet rs =ps.executeQuery();
			List<Position> positionList =toPositionList(rs);

			if(positionList.isEmpty() == true){
				return null;
			}else {
				return positionList.get(0);
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			utils.CloseableUtil.close(ps);
		}
	}

	public List<Position> getPositions(Connection connection){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM positions";
			ps = connection.prepareStatement(sql);

			ResultSet rs =ps.executeQuery();
			return toPositionList(rs);

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			utils.CloseableUtil.close(ps);
		}
	}

	private List<Position> toPositionList(ResultSet rs) throws SQLException{
		List<Position> ret = new ArrayList<Position>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Position position = new Position();
				position.setId(id);
				position.setName(name);

				ret.add(position);
			}
			return ret;
		}finally{
			utils.CloseableUtil.close(rs);
		}
	}
}
