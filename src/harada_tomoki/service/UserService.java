package harada_tomoki.service;

import java.sql.Connection;
import java.util.List;

import harada_tomoki.beans.User;
import harada_tomoki.dao.UserDao;
import utils.CipherUtil;
import utils.CloseableUtil;
import utils.DBUtil;


public class UserService {

	public boolean register(User user){

		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			// パスワードの暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			// DBへインサート
			UserDao userDao = new UserDao();
			userDao.insert(connection, user);

			utils.DBUtil.commit(connection);

			return true;
		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			utils.DBUtil.rollback(connection);
			throw e;
		}finally{
			utils.CloseableUtil.close(connection);
		}
	}

/***
 * ユーザー更新
 * @param user
 */
	public boolean update(User user){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();

//			System.out.println("userServce:update");

			//
			User whereUser = new User();
			whereUser.setId(user.getId());
//			System.out.println("userServce:userId:" + user.getId());

			// 現在のユーザー情報を取得
			User nowUser = getUser(whereUser);

//			System.out.println("userServce:now:" + nowUser.getPassword());
//			System.out.println("userServce:new:" + user.getPassword());

			// パスワードを比較
			if (!nowUser.getPassword().equals(user.getPassword())){
				String encPassword = CipherUtil.encrypt(user.getPassword());
				user.setPassword(encPassword);

//				System.out.println("userServce:update:password:" + user.getPassword());
			}

			// DBへインサート
			UserDao userDao = new UserDao();
			userDao.update(connection, user);

			DBUtil.commit(connection);
			return true;
		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public List<User> getUsers(User user) {
		Connection connection = null;
		try{

			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			List<User> users = userDao.getUsers(connection, user);

			DBUtil.commit(connection);

			return users;

		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	public List<User> getSortedUsers(List<String> sortKeys) {
		Connection connection = null;
		try{

			connection = DBUtil.getConnection();

			UserDao userDao = new UserDao();
			List<User> users = userDao.getSortedUsers(connection, sortKeys);

			DBUtil.commit(connection);

			return users;

		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}

	/***
	 * ユーザーの取得
	 * @param user
	 * @return
	 */
	public User getUser(User user){
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();

//			System.out.println("UserService:getUser");

			UserDao userDao = new UserDao();
			List<User> users = userDao.getUsers(connection, user);

//			System.out.println("UserService:users.size()=" + users.size());

			DBUtil.commit(connection);
			// サイズチェック
			if(users.isEmpty() == true){
				return null;
			} else if (2 <= users.size()){
				throw new IllegalStateException("2 <= userList.size()");
			}

			return users.get(0);

		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}
	}

}
