package harada_tomoki.filter;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import harada_tomoki.beans.User;

@WebFilter(urlPatterns = { "/maintenance","/signup","/setting"})
public class AccessFilter implements Filter {

	@Override
	public void init(FilterConfig config) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();

		User user = (User)session.getAttribute("loginUser");
		if (user == null){
			chain.doFilter(request, response);
			return;
		}

		if ((user.getBranchId() != 1) || (user.getPositionId() != 1))
		{
			session.setAttribute("errorMessages", Arrays.asList("利用できない機能です"));
			((HttpServletResponse)response).sendRedirect("./");
			return;
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ
	}

}
