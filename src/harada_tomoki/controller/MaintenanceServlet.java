package harada_tomoki.controller;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import harada_tomoki.beans.Branch;
import harada_tomoki.beans.Position;
import harada_tomoki.beans.User;
import harada_tomoki.service.BranchService;
import harada_tomoki.service.PositionService;
import harada_tomoki.service.UserService;

@WebServlet(urlPatterns = { "/maintenance" })
public class MaintenanceServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
		List<String> sortKeys = Arrays.asList("branch_id","position_id");

		List<User> users = new UserService().getSortedUsers(sortKeys);
		List<Branch> branches  = new BranchService().getBranches();
		List<Position> positions = new PositionService().getPositions();

		request.setAttribute("users", users);
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.getRequestDispatcher("maintenance.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
//		System.out.println("maintenance:doPost -begin-");

		// ログインIDを取得
		String id = request.getParameter("id");

		// 検索条件用ユーザーを作成
		User whereUser = new User();
		whereUser.setId(Integer.parseInt(id));

		// ユーザー情報の取得
		User user = new UserService().getUser(whereUser);

		// 削除フラグを反転させる
		int isDeleted = Integer.parseInt(request.getParameter("isDeleted"));
		if (isDeleted == 1){
			isDeleted = 0;
		}else{
			isDeleted = 1;
		}

		// 削除フラグ反映
		user.setIsDeleted(isDeleted);
		new UserService().update(user);

		response.sendRedirect("maintenance");
	}
}
