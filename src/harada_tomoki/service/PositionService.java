package harada_tomoki.service;

import java.sql.Connection;
import java.util.List;

import harada_tomoki.beans.Position;
import harada_tomoki.dao.PositionDao;

public class PositionService {
	public List<Position> getPositions() {
		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			// DBへインサート
			PositionDao positionDao = new PositionDao();
			List<Position> positions = positionDao.getPositions(connection);

			utils.DBUtil.commit(connection);

			return positions;
		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			return null;
		}catch(Exception e){
			utils.DBUtil.rollback(connection);
			return null;
		}finally{
			utils.CloseableUtil.close(connection);
		}
	}
}
