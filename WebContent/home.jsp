<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<!-- CSS -->

<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="./semantic/semantic.min.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/menu.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/icon.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/modal.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/modal.min.css">

<!-- script -->
<script type="text/javascript">
function check(verb){
	var message = verb + 'を削除してもよろしいですか？';

	// 確認ダイアログを表示
	if(window.confirm(message)){
		return true;
	}
	else{
		window.alert('キャンセルされました');
		return false;
	}
}
</script>
<!-- etc -->

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>
<body>
<!-- テスト -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<!-- メニュー  -->
 <div class="ui top fixed menu">
	<a href="./" class="item"><i class="home icon"></i>ホーム</a>
	<a href="post" class="item"><i class="pencil alternate icon"></i>新規投稿</a>
	<c:if test="${ loginUser.branchId == 1 && loginUser.positionId == 1 }">
		<a href="maintenance" class="item"><i class="user icon"></i>ユーザー管理</a>
	</c:if>
	<div class="right menu">
		<div id="search" class="item"><i class="search icon"></i>検索</div>
		<a href="logout" class="item"><i class="sign out alternate icon"></i>ログアウト</a>
	</div>
</div>

<!-- 検索 -->
	<div class="ui small modal">
		<div class="search-contents">
			<form action="." method="get" class="ui form">
				<h2 class="ui left floated header">検索</h2>
				<div class="ui clearing divider"></div>
				<div class="field">
					<label>カテゴリ</label>
					<input name="category" value="${ category }"><br />
				</div>
				<div class="field">
					<label>期間</label>
					<div class="two fields">
						<div class="field"><input type="date" name="fromDate" value="${ fromDate }"></div>
						<br>～
						<div class="field"><input type="date" name="toDate" value="${ toDate }"></div>
					</div>
				</div>
				<div class="ui clearing divider"></div>
				<div class="right"><input type="submit" value="検索" class="ui teal button"></div>
			</form>
		</div>
	</div>

<div class="post-contents">
<!-- エラーメッセージ  -->
<div class="error-contents">
	<c:if test="${ not empty errorMessages }">
		<div class="ui error message">
			<div class="errorMessages">
			<ul>
				<c:forEach items="${ errorMessages }" var="message">
					<li><c:out value="${ message }" />
				</c:forEach>
			</ul>
			</div>
			<c:remove var="errorMessages" scope="session"/>
		</div>
	</c:if>
</div>

<!-- 投稿  -->
<c:if test="${ posts.size() == 0 }">
	<div class="contents-title">投稿はありません</div>
</c:if>
<c:forEach items="${ posts }" var="post">
<div class="ui segment">
<div class="post-title">
	<h2 class="ui left floated header"><c:out value="${ post.subject }" /></h2>
	<!-- <a class="ui massive basic label"><c:out value="${ post.subject }" /></a> -->
</div>
<div class="ui clearing divider"></div>
	<div class="right">
	<c:forEach items="${ users }" var="user">
		<c:if test="${ post.userId == user.id }">
			<div class="ui basic label">投稿者
				<div class="detail">
					<c:out value="${user.name}" />
				</div>
			</div>
		</c:if>
	</c:forEach>
	<div class="ui basic label">投稿日
		<div class="detail">
			<fmt:formatDate value="${post.insertDate}" pattern="yyyy/MM/dd HH:mm:ss"></fmt:formatDate>
		</div>
	</div>
	<div class="ui basic label">カテゴリー
		<div class="detail">
			<a href="./?category=${ post.category }&fromDate=&toDate="><c:out value="${ post.category }" /></a>
		</div>
	</div>
	</div>
<div class="ui medium header"></div>
<h5>
<c:forEach items="${ post.splitedBody }" var="line">
	<c:out value="${ line }" /><br />
</c:forEach>
</h5>
<div class="right">
<form action="post" method="post" onSubmit="return check('投稿')">
	<c:if test="${ post.userId == loginUser.id }">
		<input type="hidden" name="postId" value="${ post.id }"><br />
		<button type="submit" class="ui mini icon button">
			<i class="trash icon"></i>
		</button>
		<!-- <input type="submit" value="投稿の削除" class="mini ui teal button"><br /> -->
	</c:if>
</form>
</div>
			<div class="ui comments">
				<h3 class="ui dividing header">コメント</h3>
				<c:forEach items="${ comments }" var="comment">
					<c:if test="${ comment.postId == post.id }">
					<div class="comment">
					<c:forEach items="${ users }" var="user">
						<label class="author">
							<c:if test="${ comment.userId == user.id }">
								<c:out value="${user.name}" />
								<div class="metadata"><span class="date">
									<fmt:formatDate value="${comment.insertDate}" pattern="yyyy/MM/dd HH:mm:ss"></fmt:formatDate>
								</span></div><br />
							</c:if>
						</label>
					</c:forEach>
					<div class="ui two column middle aligned very relaxed stackable grid">
						<div class="column">
							<c:forEach items="${ comment.splitedBody }" var="line">
								<c:out value="${ line }" /><br />
							</c:forEach>
						</div>
						<div class="column">
							<div class="right">
								<form action="comment" method="post" onSubmit="return check('コメント')">
									<c:if test="${ comment.userId == loginUser.id }">
										<input type="hidden" name="commentId" value="${ comment.id }"><br />
										<button type="submit" class="ui mini icon button">
											<i class="trash icon"></i>
										</button>
										<!-- <input type="submit" value="コメントの削除"  class="mini ui teal button"><br /> -->
									</c:if>
								</form>
							</div>
						</div>
					</div>
					</div>
				</c:if>
				</c:forEach>
			</div>
	<form action="comment" method="post" class="ui form" onSubmit="return check(${'post.id'})">
		<div class="field">
			<input type="hidden" name="postId" value="${ post.id }"><br />
			<label>コメント(500文字まで)</label>
			<textarea id="commentarea-${ post.id }" name="body" cols="100" rows="5" class="tweet-box"></textarea><br />
			<!-- <div id="poplabel-${ post.id }" class="ui pointing red basic label">500文字以内で入力してください</div> -->
		</div>
		<div class="field">
			<div class="right"><button type="submit" class="ui mini teal button"><i class="comment icon"></i>コメント</button></div>
			<!-- <div class="right"><input id="commentsubmit-${ post.id }" type="submit" value="コメント" class="ui teal button"></input></div> -->
		</div>
	</form>
</div>
</c:forEach>
<div class="center"><div class="copyright">Copyright(c)Tomoki Harada</div></div>
</div>

<script src="./semantic/semantic.min.js"></script>
<script src="./semantic/components/modal.min.js"></script>
<script>
	$('#search').click(function() {
		$('.small.modal').modal('show');
	});
</script>
<script>
function checklength(id){
	var textareaid = "#commentarea-" + id;
	var poplabelid = "#poplabel" + id;
	var count = $(textareaid).val().length;
	if (count <= 500){
		$(poplabelid).hide();
		return false;
	}else{
		$(poplabelid).show();
		return false;
	}
}
</script>
</body>
</html>