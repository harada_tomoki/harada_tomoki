package harada_tomoki.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

@WebFilter("/*")
public class EncodingFilter implements Filter {
	private static String INIT_PARAMATER_NAME_ENCODING = "encoding";
	private static String DEFAULT_ENCODING = "UTF-8";
	private String encoding;

	@Override
	public void init(FilterConfig config) throws ServletException {
 		encoding = config.getInitParameter(INIT_PARAMATER_NAME_ENCODING);

		if(encoding == null){
			encoding = DEFAULT_ENCODING;
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		if (request.getCharacterEncoding() == null){
			request.setCharacterEncoding(encoding);
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}

}
