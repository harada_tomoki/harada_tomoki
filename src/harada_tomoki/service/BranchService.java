package harada_tomoki.service;

import java.sql.Connection;
import java.util.List;

import harada_tomoki.beans.Branch;
import harada_tomoki.dao.BranchDao;

public class BranchService {
	public List<Branch> getBranches() {
		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			// DBへインサート
			BranchDao branchDao = new BranchDao();
			List<Branch> branchs = branchDao.getBranchs(connection);

			utils.DBUtil.commit(connection);

			return branchs;
		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			return null;
		}catch(Exception e){
			utils.DBUtil.rollback(connection);
			return null;
		}finally{
			utils.CloseableUtil.close(connection);
		}
	}

}
