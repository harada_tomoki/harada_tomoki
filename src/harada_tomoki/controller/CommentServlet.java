package harada_tomoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import harada_tomoki.beans.Comment;
import harada_tomoki.beans.User;
import harada_tomoki.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
		response.sendRedirect("./");
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

//		System.out.println("/comment/doPost - begin");

		// コメントIDを取得
		String commentId = request.getParameter("commentId");

		// 投稿IDを取得
		String postId = request.getParameter("postId");
		String body = request.getParameter("body");

		// ユーザー情報の取得
		User loginUser = (User)request.getSession().getAttribute("loginUser");
		int userId = loginUser.getId();

		// エラーメッセージ格納先
		List<String> messages = new ArrayList<String>();

		// コメント投稿の場合
		if ((postId != null) && (!postId.isEmpty())){
			Comment comment = new Comment();
			comment.setPostId(Integer.parseInt(postId));
			comment.setBody(body);
			comment.setUserId(userId);

			if (isValid(messages, comment)){
				new CommentService().regist(comment);
			}
		}

		// コメント削除の場合
		if ((commentId != null) && (!commentId.isEmpty())){
			new CommentService().delete(Integer.parseInt(commentId));
		}

		// セッションにエラーメッセージを格納してリダイレクト
		HttpSession session = request.getSession();
		session.setAttribute("errorMessages", messages);
		response.sendRedirect("./");

//		System.out.println("/comment/doPost - end");
	}

	private boolean isValid(List<String> messages,Comment comment){


		// 文字数500以上の場合
		if (comment.getBody().length() > 500){
			messages.add("コメントの本文は500文字以下で入力してください。");
		}

		if (comment.getBody().trim().length() == 0){
			messages.add("コメントの本文を入力してください。");
		}

		// エラーメッセージが0件より多いならエラー
		if (messages.size() > 0){
			return false;
		}

		return true;
	}
}
