package utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexUtil {
	public static boolean isMatch(String word,String regex) {
		Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(word);

        return matcher.matches();
	}
}
