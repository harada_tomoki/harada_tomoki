package harada_tomoki.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import harada_tomoki.beans.Post;
import harada_tomoki.exception.SQLRuntimeException;

public class PostDao {
	public void insert(Connection connection,Post post){

		System.out.println("InsertStart");
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO posts (");
			sql.append("  subject");
			sql.append(", body");
			sql.append(", category");
			sql.append(", user_id");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(")VALUES(");
			sql.append("?");	// subject
			sql.append(", ?");	// body
			sql.append(", ?");	// category
			sql.append(", ?");	// userId
			sql.append(", CURRENT_TIMESTAMP()");
			sql.append(", CURRENT_TIMESTAMP()");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1, post.getSubject());
			ps.setString(2, post.getBody());
			ps.setString(3, post.getCategory());
			ps.setInt(4, post.getUserId());

//			System.out.println(sql.toString());

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
			utils.CloseableUtil.close(ps);
		}
	}

	public void delete(Connection connection,int id) {
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM posts WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

//			System.out.println(sql.toString());

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
			utils.CloseableUtil.close(ps);
		}
	}

	public List<Post> getPosts(Connection connection,String category,String fromDate, String toDate)
	{

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();

			sql.append("SELECT * FROM posts ");
			sql.append(" WHERE insert_date BETWEEN ?");
			sql.append("   AND ? ");
			sql.append("   AND category LIKE ? ");
			sql.append(" ORDER BY insert_date DESC");

			// 渡されたユーザーを元に条件追加
			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, fromDate);
			ps.setString(2, toDate + " 23:59:59");
			ps.setString(3, "%" + category + "%");

			ResultSet rs =ps.executeQuery();

			return toPostList(rs);

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			utils.CloseableUtil.close(ps);
		}
	}

	private List<Post> toPostList(ResultSet rs) throws SQLException{
		List<Post> ret = new ArrayList<Post>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String subject = rs.getString("subject");
				String body = rs.getString("body");
				int userId = rs.getInt("user_id");
				String category = rs.getString("category");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				Timestamp updateDate = rs.getTimestamp("update_date");

				Post post = new Post();
				post.setId(id);
				post.setSubject(subject);
				post.setBody(body);
				post.setUserId(userId);
				post.setCategory(category);
				post.setInsertDate(insertDate);
				post.setUpdateDate(updateDate);
				post.setSplitedBody(post.getBody().split("\n"));

				ret.add(post);
			}
			return ret;
		}finally{
			utils.CloseableUtil.close(rs);
		}
	}
}
