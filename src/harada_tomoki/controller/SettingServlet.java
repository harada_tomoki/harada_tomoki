package harada_tomoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import harada_tomoki.beans.Branch;
import harada_tomoki.beans.Position;
import harada_tomoki.beans.User;
import harada_tomoki.service.BranchService;
import harada_tomoki.service.PositionService;
import harada_tomoki.service.UserService;

@WebServlet(urlPatterns = { "/setting" })
public class SettingServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
		System.out.println("SettingServlet:doGet");

		// ユーザーID
		String userId = request.getParameter("id");

		System.out.println("SettingServlet:doGet:userId:" + userId);

		// セッションの取得
		HttpSession session = request.getSession();

		// 正規表現によるパラメータチェック
		String userIdPattern = "^[0-9]+$";
		Pattern pattern = Pattern.compile(userIdPattern);
		Matcher matcher = pattern.matcher(userId);
		System.out.println("SettingServlet:doGet:matcher:" + matcher.matches());
		if ((userId == null) || (!matcher.matches())){
			List<String> messages = new ArrayList<String>();
			messages.add("不正なパラメータが指定されました。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./maintenance");
			return;
		}

		// SQL条件用のユーザーを生成
		User whereUser = new User();
		whereUser.setId(Integer.parseInt(userId));

		// ユーザーを取得
		User user = new UserService().getUser(whereUser);
		if (user == null){
//			System.out.println("SettingServlet:None Id");
			List<String> messages = new ArrayList<String>();
			messages.add("不正なパラメータが指定されました。");
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./maintenance");
			return;
		}

		// 支店・部署・役職表示用
		List<Branch> branches = new BranchService().getBranches();
		List<Position> positions = new PositionService().getPositions();
		request.setAttribute("branches", branches);
		request.setAttribute("positions", positions);
		request.setAttribute("user", user);

		// フォワード
		request.getRequestDispatcher("setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

		// エラーメッセージ
		List<String> messages = new ArrayList<String>();

		// パスワード比較用
		String password = request.getParameter("password");
		String validPassword = request.getParameter("validPassword");

		// ユーザーID
		String userId = request.getParameter("userId");

		// 支店番号
		String branchId = request.getParameter("branchId");
		String positionId = request.getParameter("positionId");

		// SQL条件用のユーザーを生成
		User whereUser = new User();

		whereUser.setId(Integer.parseInt(userId));

		// ユーザーを取得
		User editUser = new UserService().getUser(whereUser);

		System.out.println("SettingServlet:doPost:1");

		// ログインIDは変更しない
		editUser.setLoginName(request.getParameter("loginName"));
		editUser.setName(request.getParameter("name"));

		if (branchId != null){
			editUser.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		}

		if (positionId != null){
			editUser.setPositionId(Integer.parseInt(request.getParameter("positionId")));
		}

		System.out.println("SettingServlet:doPost:2");

		// 既存のパスワードを取得
		if (password.length() != 0){
			editUser.setPassword(password);
		}

		System.out.println("SettingServlet:doPost:3");

		// ユーザー情報の検証
		if (isValid(messages, editUser, validPassword)){
			System.out.println("SettingServlet:doPost:4");

			new UserService().update(editUser);
			response.sendRedirect("./maintenance");
		}else{
			System.out.println("SettingServlet:doPost:5");

			// 支店・部署・役職表示用
			List<Branch> branches = new BranchService().getBranches();
			List<Position> positions = new PositionService().getPositions();
			request.setAttribute("branches", branches);
			request.setAttribute("positions", positions);

			System.out.println("SettingServlet:doPost:6");

			// セッションにエラーメッセージを格納
			HttpSession session = request.getSession();
			session.setAttribute("errorMessages", messages);

			System.out.println("SettingServlet:doPost:7");

			// 編集中のユーザー情報を設定
			request.setAttribute("user",editUser);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}

	private boolean isValid(List<String> messages,User user,String validPassword){
//		String loginNamePattern = "^[a-zA-Z0-9_\\-.]{6,20}$";
//		String passwordPattern = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$";

		// パターン
		String loginNamePattern = "^[a-zA-Z0-9_\\-.]{6,20}$";
		String passwordPattern = "^(?=.[-_@+*;:#$%&\\w]).{6,20}$";

		// 検証対象
		String loginName = user.getLoginName();
		String password = user.getPassword();
		String name = user.getName();

		// 検証用変数
		Pattern pattern;
		Matcher matcher;

		// ログインIDの検証
		pattern = Pattern.compile(loginNamePattern);
		matcher = pattern.matcher(loginName);
		if(!matcher.matches()){
			messages.add("ログインIDは半角文字で6文字以上20文字以下で入力してください");
		}

		// SQL条件用のユーザーを生成
		User whereUser = new User();
		whereUser.setLoginName(loginName);
//		if ((loginName.length() > 0) && (new UserService().getUsers(whereUser).size() > 0)){
//			messages.add("入力されたログインIDは使用できません");
//		}

		List<User> validUsers = new UserService().getUsers(whereUser);
		boolean notMachIs = false;
		// 同じログインIDがいた場合はエラー
		for (int i = 0;i < validUsers.size();i++){
			notMachIs = (validUsers.get(0).getId() != user.getId());
		}
		if (notMachIs){
			messages.add("入力されたログインIDは使用できません");
		}

		//
//		System.out.println("パスワード:" + validPassword);

		// パスワードの検証
		pattern = Pattern.compile(passwordPattern);
		matcher = pattern.matcher(password);
		if((!matcher.matches()) && (validPassword.length() != 0)){
			messages.add("パスワードは記号を含む全ての半角文字で6文字以上20文字以下で入力してください");
		}

		if((!password.equals(validPassword)) && (validPassword.length() != 0)){
			messages.add("パスワードとパスワード(確認用)が一致しません");
		}

		// 名前の検証
		if (name.length() > 10){
			messages.add("名称が不正です");
			messages.add("名称は10文字以下で入力してください");
		}

		// エラーメッセージが0件より多いならエラー
		if (messages.size() > 0){
			return false;
		}

		return true;
	}
}
