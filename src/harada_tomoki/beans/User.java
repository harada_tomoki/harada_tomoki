package harada_tomoki.beans;

import java.util.Date;

public class User {
	// フィールド
	private int id = 0;
	private String loginName = "";
	private String name = "";
	private String password = "";
	private int branchId = 0;
	private int positionId = 0;
	private Date insertDate;
	private Date updateDate;
	private int isDeleted;

	// getter
	public int getId() {
		return id;
	}
	public String getLoginName() {
		return loginName;
	}
	public String getName() {
		return name;
	}
	public String getPassword() {
		return password;
	}
	public int getBranchId() {
		return branchId;
	}
	public int getPositionId() {
		return positionId;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public int getIsDeleted() {
		return isDeleted;
	}

	// setter
	public void setId(int id) {
		this.id = id;
	}
	public void setLoginName(String loginId) {
		this.loginName = loginId;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public void setPositionId(int positionId) {
		this.positionId = positionId;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public void setIsDeleted(int isDeleted) {
		this.isDeleted = isDeleted;
	}

}
