package harada_tomoki.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import harada_tomoki.beans.Comment;
import harada_tomoki.exception.SQLRuntimeException;

public class CommentDao {
	public void insert(Connection connection,Comment comment){

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments (");
			sql.append("  post_id");
			sql.append(", body");
			sql.append(", user_id");
			sql.append(", insert_date");
			sql.append(", update_date");
			sql.append(")VALUES(");
			sql.append("?");	// subject(件名)
			sql.append(", ?");	// body(件名)
			sql.append(", ?");	// userId(ユーザーID)
			sql.append(", CURRENT_TIMESTAMP()");
			sql.append(", CURRENT_TIMESTAMP()");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, comment.getPostId());
			ps.setString(2, comment.getBody());
			ps.setInt(3, comment.getUserId());

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
			utils.CloseableUtil.close(ps);
		}
	}

	public void delete(Connection connection,int id) {
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("DELETE FROM comments WHERE id = ?");

			ps = connection.prepareStatement(sql.toString());
			ps.setInt(1, id);

			System.out.println(sql.toString());

			ps.executeUpdate();

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		}finally {
			utils.CloseableUtil.close(ps);
		}
	}

	public List<Comment> getComments(Connection connection) {
		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();

			sql.append(" SELECT * FROM comments ORDER BY insert_date");

			// 渡されたユーザーを元に条件追加
//			List<String> whereValueList = new ArrayList<String>();

			ps = connection.prepareStatement(sql.toString().replaceFirst("AND", "WHERE"));

//			System.out.println("whereValueList.size = " + whereValueList.size());

			ResultSet rs =ps.executeQuery();

			return toCommentList(rs);

		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			utils.CloseableUtil.close(ps);
		}
	}

	public List<Comment> toCommentList(ResultSet rs) throws SQLException{
		List<Comment> ret = new ArrayList<Comment>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				int postId = rs.getInt("post_id");
				String body = rs.getString("body");
				int userId = rs.getInt("user_id");
				Timestamp insertDate = rs.getTimestamp("insert_date");
				Timestamp updateDate = rs.getTimestamp("update_date");

				Comment comment = new Comment();
				comment.setId(id);
				comment.setPostId(postId);
				comment.setBody(body);
				comment.setUserId(userId);
				comment.setInsertDate(insertDate);
				comment.setUpdateDate(updateDate);
				comment.setSplitedBody(comment.getBody().split("\n"));

				ret.add(comment);
			}
			return ret;
		}finally{
			utils.CloseableUtil.close(rs);
		}
	}
}
