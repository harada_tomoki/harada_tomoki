<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="./semantic/semantic.css">
<link rel="stylesheet" type="text/css" href="./semantic/semantic.min.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/reset.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/site.css">

<link rel="stylesheet" type="text/css" href="./semantic/components/container.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/grid.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/header.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/image.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/menu.css">

<link rel="stylesheet" type="text/css" href="./semantic/components/divider.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/segment.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/form.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/input.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/button.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/list.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/message.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/icon.css">
<script src="/semantic/semantic.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板 - ログイン</title>
</head>
<body>
<div class="login-contents">
<div class="ui middle aligned center aligned grid">
  <div class="column">
    <h2 class="ui teal image header">
      <div class="content">
        社内掲示板
      </div>
    </h2>
    <c:if test="${ not empty errorMessages }">
	<div class="ui error message">
		<c:forEach items="${ errorMessages }" var="message">
			<c:out value="${ message }" />
		</c:forEach>
	</div>
	<c:remove var="errorMessages" scope="session"/>
	</c:if>
    <form action="login" method="post" class="ui large form">
      <div class="ui stacked segment">
        <div class="field">
          <div class="ui left icon input">
            <i class="user icon"></i>
            <input  type="text" name="loginName" value="${ loginName }" id="loginName" placeholder="ログインID" /><br />
          </div>
        </div>
        <div class="field">
          <div class="ui left icon input">
            <i class="lock icon"></i>
            <input type="password" name="password" value="${ password }" id="password" placeholder="パスワード" /><br />
          </div>
        </div>
        <button type="submit" class="ui large teal button"><i class="sign in alternate icon"></i>ログイン</button>
        <!-- <input type="submit" value="ログイン" class="ui fluid large teal submit button"><br /> -->
      </div>
    </form>
  </div>
</div>

<br />
<div class="copyright">Copyright(c)Tomoki Harada</div>
</div>
</body>
</html>