package harada_tomoki.filter;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import harada_tomoki.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {

	@Override
	public void init(FilterConfig config) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ
		System.out.println("LoginFilter:init");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
		throws IOException, ServletException {

		// 現在のセッションとURLを取得
		HttpSession session = ((HttpServletRequest)request).getSession();
		String servletPath = ((HttpServletRequest)request).getServletPath();

		System.out.println("LoginFilter:doFilter:servletPath:"+servletPath);

		// ログイン画面の場合はフィルターしない
		if (servletPath.contains("/login") ||
			servletPath.contains("/css") ||
			servletPath.contains("/semantic")){

			chain.doFilter(request, response);
			return;
		}

		// ログインしていない場合の処理
		User user = (User) session.getAttribute("loginUser");
		if (user == null){
			session.setAttribute("errorMessages", Arrays.asList("ログインしてください"));
			((HttpServletResponse)response).sendRedirect("./login");
			return;
		}

		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ
	}
}
