package harada_tomoki.service;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import harada_tomoki.beans.Post;
import harada_tomoki.dao.PostDao;
import utils.CloseableUtil;
import utils.DBUtil;

public class PostService {
	public boolean regist(Post post) {
		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			// DBへインサート
			PostDao postDao = new PostDao();
			postDao.insert(connection, post);

			utils.DBUtil.commit(connection);

			return true;
		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			utils.DBUtil.rollback(connection);
			throw e;
		}finally{
			utils.CloseableUtil.close(connection);
		}
	}

	public boolean delete(int id){
		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			// DBへインサート
			PostDao postDao = new PostDao();
			postDao.delete(connection, id);

			utils.DBUtil.commit(connection);

			return true;
		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			utils.DBUtil.rollback(connection);
			throw e;
		}finally{
			utils.CloseableUtil.close(connection);
		}
	}

	public List<Post> getPosts(String category,String fromDate, String toDate) {
		Connection connection = null;
		try{
			connection = DBUtil.getConnection();

			if (category == null){
				category = "";
			}

			if ((fromDate == null) || (fromDate.length() == 0)){
				fromDate = "2018-01-01";
			}

			if ((toDate == null) || (toDate.length() == 0)){
				SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
				toDate = simpleDateFormat.format(new Date());
			}


			PostDao postDao = new PostDao();
			List<Post> posts = postDao.getPosts(connection,category,fromDate,toDate);

			DBUtil.commit(connection);

			return posts;

		}catch(RuntimeException e){
			DBUtil.rollback(connection);
			throw e;
		}catch(Exception e){
			DBUtil.rollback(connection);
			throw e;
		}finally{
			CloseableUtil.close(connection);
		}

	}
}
