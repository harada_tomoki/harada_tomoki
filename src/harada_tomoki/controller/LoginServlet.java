package harada_tomoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import harada_tomoki.beans.User;
import harada_tomoki.service.LoginService;

@WebServlet(urlPatterns={ "/login" })
public class LoginServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{
		request.getRequestDispatcher("/login.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

		// 画面から入力情報を取得
		String loginName = request.getParameter("loginName");
		String password = request.getParameter("password");

		LoginService loginService = new LoginService();
		User user = loginService.login(loginName,password);

		// System.out.println(user.getName());

		HttpSession session = request.getSession();
		if ((user != null) && (user.getIsDeleted() == 0)){
			session.setAttribute("loginUser", user);
			response.sendRedirect("./");
		}else{
			// セッションに情報を格納
			List<String> messages = new ArrayList<String>();
			messages.add("ログインに失敗しました。");
			session.setAttribute("errorMessages", messages);
			session.setAttribute("loginName", loginName);

			response.sendRedirect("./login");
		}
	}
}
