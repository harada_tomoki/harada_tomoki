package harada_tomoki.beans;

public class Position {
	private int id;
	private String name;

	// getter
	public int getId() {
		return id;
	}
	public String getName() {
		return name;
	}

	// getter
	public void setId(int id) {
		this.id = id;
	}
	public void setName(String name) {
		this.name = name;
	}
}
