package harada_tomoki.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import harada_tomoki.beans.Post;
import harada_tomoki.beans.User;
import harada_tomoki.service.PostService;

@WebServlet(urlPatterns = { "/post" })
public class PostServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

		request.getRequestDispatcher("post.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response)
			throws ServletException,IOException{

		String postId = request.getParameter("postId");

		// エラーメッセージ
		List<String> messages = new ArrayList<String>();

		// セッション
		HttpSession session = request.getSession();

		if ((postId != null) && (!postId.isEmpty())){
			new PostService().delete(Integer.parseInt(postId));

		}else{
			// セッションからログインユーザーを取得
			User loginUser = (User)session.getAttribute("loginUser");

			// 投稿クラスを生成
			Post post = new Post();

			post.setSubject(request.getParameter("subject"));
			post.setBody(request.getParameter("body"));
			post.setCategory(request.getParameter("category"));
			post.setUserId(loginUser.getId());

			// このあたりでバリデーション
			if (!isValid(messages, post)){
				request.setAttribute("subject", post.getSubject());
				request.setAttribute("body", post.getBody());
				request.setAttribute("category", post.getCategory());
				request.setAttribute("errorMessages", messages);

				request.getRequestDispatcher("post.jsp").forward(request, response);
				return;
			}
			new PostService().regist(post);
		}

		// ホームへ戻る
		response.sendRedirect("./");
	}

	private boolean isValid(List<String> messages,Post post){

		// 件名
		if (post.getSubject().length() > 30){
			messages.add("件名は30文字以内で入力してください");
		}

		if (post.getSubject().trim().length() == 0){
			messages.add("件名を入力してください");
		}

		// 文字数1000以上の場合
		if (post.getBody().length() > 1000){
			messages.add("本文は1000文字以内で入力してください。");
		}

		if (post.getBody().trim().length() == 0){
			messages.add("本文を入力してください。");
		}

		// カテゴリ
		if (post.getCategory().length() > 10){
			messages.add("カテゴリは10文字以内で入力してください");
		}

		if (post.getCategory().trim().length() == 0){
			messages.add("カテゴリを入力してください");
		}

		// エラーメッセージが0件より多いならエラー
		if (messages.size() > 0){
			return false;
		}

		return true;
	}
}
