package harada_tomoki.service;

import java.sql.Connection;
import java.util.List;

import harada_tomoki.beans.User;
import harada_tomoki.dao.UserDao;
import utils.CipherUtil;

public class LoginService {

	public User login(String loginName,String password){
		Connection connection = null;
		try{
			connection = utils.DBUtil.getConnection();

			UserDao userDao = new UserDao();
			String encPassword = CipherUtil.encrypt(password);

			if ((loginName.length() == 0)){
				return null;
			}

			// 検索条件用ユーザーを生成
			User whereUser = new User();
			whereUser.setLoginName(loginName);

			List<User> users = userDao.getUsers(connection,whereUser);
			utils.DBUtil.commit(connection);

			// サイズチェック
			if(users.isEmpty() == true){
				return null;
			} else if (2 <= users.size()){
				throw new IllegalStateException("2 <= userList.size()");
			}

			// 結果用ユーザー
			User resultUser = users.get(0);

//			System.out.println("LoginService:" + resultUser.getPassword());
//			System.out.println("LoginService:" + encPassword);

			// パスワードのチェック
			if (resultUser.getPassword().equals(encPassword)){
				return resultUser;
			}else{
				return null;
			}

		}catch(RuntimeException e){
			utils.DBUtil.rollback(connection);
			throw e;

		}catch (Exception e) {
			utils.DBUtil.rollback(connection);
			throw e;
		} finally {
			utils.CloseableUtil.close(connection);
		}
	}
}
