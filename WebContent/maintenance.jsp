<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="./semantic/semantic.min.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/menu.css">
<link rel="stylesheet" type="text/css" href="./semantic/components/icon.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理</title>
<script type="text/javascript">

function check(verb){
	var message = verb + 'してよろしいですか？';

	// 確認ダイアログを表示
	if(window.confirm(message)){
		return true;
	}
	else{
		window.alert('キャンセルされました');
		return false;
	}
}

</script>
</head>
<body>
<div class="ui top fixed menu">
	<a href="./" class="item"><i class="home icon"></i>ホーム</a>
	<a href="signup" class="item"><i class="user circle icon"></i>登録</a>
	<div class="right menu">
		<a href="logout" class="item"><i class="sign out alternate icon"></i>ログアウト</a>
	</div>
</div>
<div class="main-contents">
<div class="error-contents">
<c:if test="${ not empty errorMessages }">
	<div class="ui error message">
	<ul>
		<c:forEach items="${ errorMessages }" var="message">
			<li><c:out value="${ message }" />
		</c:forEach>
	</ul>
	</div>
	<c:remove var="errorMessages" scope="session"/>
</c:if>
</div>
<div class="ui segment">
	<h2 class="ui left floated header">ユーザー管理</h2>
<div class="ui clearing divider"></div>
<table id="foo-table" class="ui celled table">
	<thead>
		<tr>
			<th>アカウント名</th>
			<th>名称</th>
			<th>支店</th>
			<th>部署・役職</th>
			<th>停止/復活</th>
		</tr>
	</thead>
	<tbody>
	<c:forEach items="${ users }" var="user">
		<tr>
			<td>
				<label>${ user.loginName }</label>
			</td>
			<td>
				<a href="setting?id=${ user.id }">${ user.name }</a><br />
			</td>
			<td>
				<c:forEach items="${ branches }" var="branch">
					<c:if test="${ branch.id == user.branchId }">
						<label>${ branch.name }</label>
					</c:if>
				</c:forEach>
			</td>
			<td>
				<c:forEach items="${ positions }" var="position">
					<c:if test="${ position.id == user.positionId }">
						<label>${ position.name }</label>
					</c:if>
				</c:forEach>
			</td>
			<td>
				<c:if test="${ user.id != loginUser.id }">
				<form action="maintenance" method="post" onSubmit="return check(${ 'executeMode.value' })">
					<input type="hidden" name="id" value="${ user.id }">
					<input type="hidden" name="loginName" value="${ user.loginName }">
					<input type="hidden" name="isDeleted" value="${ user.isDeleted }">
					<c:if test="${ user.isDeleted == 0}"><input type="submit" name="executeMode" value="停止" class="ui teal button"/><br /></c:if>
					<div class="right"><c:if test="${ user.isDeleted == 1}"><input type="submit" name="executeMode" value="復活" class="ui red button"/><br /></c:if></div>
				</form>
				</c:if>
			</td>
		</tr>
	</c:forEach>
	</tbody>
	<!--
	<tfoot>
	<tr>
		<th colspan="3">
		<div class="ui right floated pagination menu">
			<a class="icon item">
				<i class="left chevron icon"></i>
			</a>
			<a class="item">1</a>
			<a class="item">2</a>
			<a class="item">3</a>
			<a class="item">4</a>
			<a class="icon item">
			<i class="right chevron icon"></i>
		</a>
		</div>
		</th>
		</tr>
	</tfoot>
	 -->
</table>
</div>

<div class="copyright">Copyright(c)Tomoki Harada</div>
</div>
<br />
</html>