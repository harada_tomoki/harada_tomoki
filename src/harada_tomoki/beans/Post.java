package harada_tomoki.beans;

import java.util.Date;

public class Post {
	private int id;
	private String subject;
	private String body;
	private String category;
	private int userId;
	private Date insertDate;
	private Date updateDate;

	// 改行表示用
	private String[] splitedBody;

	// getter
	public int getId() {
		return id;
	}
	public String getSubject() {
		return subject;
	}
	public String getBody() {
		return body;
	}
	public String getCategory() {
		return category;
	}
	public int getUserId() {
		return userId;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public String[] getSplitedBody() {
		return splitedBody;
	}

	// setter
	public void setId(int id) {
		this.id = id;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public void setSplitedBody(String[] splitedBody) {
		this.splitedBody = splitedBody;
	}

}
