package harada_tomoki.beans;

import java.util.Date;

public class Comment {
	private int id;
	private int postId;
	private String body;
	private int userId;
	private Date insertDate;
	private Date updateDate;

	// 表示用
	private String[] splitedBody;

	// getter
	public int getId() {
		return id;
	}
	public int getPostId() {
		return postId;
	}
	public String getBody() {
		return body;
	}
	public int getUserId() {
		return userId;
	}
	public Date getInsertDate() {
		return insertDate;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public String[] getSplitedBody() {
		return splitedBody;
	}

	// setter
	public void setId(int id) {
		this.id = id;
	}
	public void setPostId(int postId) {
		this.postId = postId;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public void setInsertDate(Date insertDate) {
		this.insertDate = insertDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	public void setSplitedBody(String[] splitedBody) {
		this.splitedBody = splitedBody;
	}

}
