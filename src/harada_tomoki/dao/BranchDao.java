package harada_tomoki.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import harada_tomoki.beans.Branch;
import harada_tomoki.exception.SQLRuntimeException;

public class BranchDao {

	public Branch getBranch(Connection connection,String name){

		PreparedStatement ps = null;
		try{
			String sql = "SELECT * FROM branches";
			ps = connection.prepareStatement(sql);

			ResultSet rs =ps.executeQuery();
			List<Branch> branchList =toBranchList(rs);

			if(branchList.isEmpty() == true){
				return null;
			}else {
				return branchList.get(0);
			}
		}catch(SQLException e){
			throw new SQLRuntimeException(e);
		} finally{
			utils.CloseableUtil.close(ps);
		}
	}

	public List<Branch> getBranchs(Connection connection){

		PreparedStatement ps = null;

		try{

			String sql = "SELECT * FROM branches";
			ps = connection.prepareStatement(sql);

			ResultSet rs =ps.executeQuery();
			return toBranchList(rs);

		}catch(SQLException e){
			throw new SQLRuntimeException(e);

		} finally{
			utils.CloseableUtil.close(ps);

		}
	}

	private List<Branch> toBranchList(ResultSet rs) throws SQLException{
		List<Branch> ret = new ArrayList<Branch>();
		try{
			while (rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setName(name);

				ret.add(branch);
			}
			return ret;
		}finally{
			utils.CloseableUtil.close(rs);
		}
	}
}
